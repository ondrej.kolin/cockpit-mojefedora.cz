function parseMojefedoraFeed(feed) {
    // Funkce bere jeden parameter a to je textový feed
    // Ten by měl být validní XML jako řetězec
    // https://www.w3schools.com/xml/xml_parser.asp
    parser = new DOMParser();
    xmlDoc = parser.parseFromString(feed, "text/xml");
    console.log(xmlDoc);
    // Najdeme si číslo posledního sestavení RSS
    const lastBuild = new Date(xmlDoc.querySelector("lastBuildDate").textContent);
    // Projdeme nabídnuté články a sestavíme z nich nějaké objekty
    const articles = [];
    // Z článku si sestavíme objekty, které pak prostě nějak necháme vyrenderovat. :-)
    xmlDoc.querySelectorAll("rss channel item").forEach(articleData => {
        const article = {
            title: articleData.querySelector("title").textContent,
            // querySelector nemá rád dvojtečky. Takže je musíte escapovat.
            // No a protože se mi to hned nepovedlo, tak ho opustím a použiji jinou funkci
            creator: articleData.getElementsByTagName("dc:creator")[0].textContent,
            comments: articleData.getElementsByTagName("slash:comments")[0].textContent,
            link: articleData.querySelector("link").textContent,
            comments_link: articleData.querySelector("comments").textContent,
            pub_date: new Date(articleData.querySelector("pubDate").textContent),
            // V popisku dostanete i odkaz na článek, ale nebude fungovat, protože cockpit odkazy, které nevedou do nové záložky blokuje :-(
            // To by šlo vyřešit doplnění _blank do všech odkazů, ale to už je nad rámec našeho článku. 
            description: articleData.querySelector("description").textContent,
        }
        articles.push(article);
    });
    // Vrátíme sestavený objekt.
    return {
        lastBuild,
        articles
    };
}

function renderMojefedoraArticle(article) {
    // jeden parametr, clanek z mojefedora.cz
    // a vrati HTML reprezentaci toho clanku.
    // Zranitelné pomocí XSS.
    return `<article>
        <h2><a target="_blank" href="${article.link}">${article.title}</a></h2>
        <div class="creator">Autor: ${article.creator}</div>
        <div class="pubDate">Vydáno: ${article.pub_date.toLocaleDateString()} ${article.pub_date.toLocaleTimeString()}</div>
        <div class="comments"><a href="${article.comments_link}">Komentářů: ${article.comments}</a></div>
        <div class="description">${article.description}</div>
    </article>`
}

function renderMojefedoraFeed(target, feedObject) {
    // funkce bere dva parametry
    // - HTML prvek do kterého se bude renderovat
    // - zbastlený objekt z parseMojefedoraFeed
    target.innerHTML = `
        <p>Poslední aktualizace: ${feedObject.lastBuild.toLocaleDateString()} ${feedObject.lastBuild.toLocaleTimeString()}</p>
        <div class="articles">
            ${feedObject.articles.map(a => renderMojefedoraArticle(a)).join("\n")}
        </div>
    `
}





// Náš web pochopitelně běží na https -> port 443
// Cockpitu ale musíte dát vědět, že se má pokusit o zabezpečené spojení
// To říká existující atribut `tls`
const session = cockpit.http(443, {address: "mojefedora.cz", tls: {}});
// Cockpit pracuje pomocí "Promise"
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
// Proto nevěřte žádnému, protože pořád jenom slibují. 
session.get("/feed/").then(data => {
    // chci přečist data streamu...
    try {
        // Přečtená data musíme zpracovat ... 
        // ... když to nevyjde hodíme chybu.
        const parsedFeed = parseMojefedoraFeed(data);
        renderMojefedoraFeed(document.getElementById("mojefedoraTarget"), parsedFeed);
    }
    catch (error) {
        window.alert("To je nemilé! Někde se stala chyba. Podívej se do konzole")
        console.error(error)
    }
}).catch(error => {
    //... a když to nevyjde, tak chci vidět alespoň error.
    console.error(error);
});